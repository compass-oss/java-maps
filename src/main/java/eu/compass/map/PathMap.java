package eu.compass.map;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Finds data in map by path with dots
 */
public class PathMap {

    protected final Map<String, Object> map;

    public PathMap(Map<String, Object> map) {
        this.map = map;
    }

    /**
     * Contains key value can be null
     *
     * @param key Key to find
     * @return True if key exist otherwise false
     */
    public boolean containsKey(String key) {
        return existWithValue(key).getLeft();
    }

    /**
     * Gets value
     *
     * @param key Search value by key
     * @return Founds value
     */
    public Optional<Object> getValue(String key) {
        return Optional.ofNullable(existWithValue(key).getRight());
    }

    public Map<String, Object> getMap() {
        return map;
    }

    @SuppressWarnings("unchecked")
    protected Pair<Boolean, Object> existWithValue(String key) {
        if (!key.contains(".")) {
            return Pair.of(map.containsKey(key), map.get(key));
        }

        Map<String, Object> actualData = map;
        List<String> parts = Arrays.asList(key.split("\\."));

        for (int i = 0; i < parts.size() && actualData != null; i++) {
            for (int j = parts.size(); j > i; j--) {
                String partialKey = String.join(".", parts.subList(i, j));
                if (!actualData.containsKey(partialKey)) {
                    continue;
                }

                Object value = actualData.get(partialKey);
                if (j == parts.size()) {
                    return Pair.of(true, value);
                }
                if (value == null || !(value instanceof Map)) {
                    return Pair.of(false, null);
                }

                actualData = (Map<String, Object>) value;
                break;
            }
        }
        return Pair.of(false, null);
    }

    @Override
    public String toString() {
        return "PathMap{" +
                "map=" + map +
                '}';
    }
}
