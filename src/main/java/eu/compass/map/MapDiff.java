package eu.compass.map;

import static java.util.stream.Collectors.toSet;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Provides Diff functionality on maps
 */
public final class MapDiff {

    private MapDiff() {

    }

    /**
     * Creates diff between two maps
     *
     * Using equals on entry set (which is basically {@code key.equals() && value.equals()} obtains old values
     * that are different from current state.
     *
     * If a new map contains new value, output contains particular key with {@code null} value. If a new map
     * does not contain a key which is in old map, output contains deleted key with old value.
     *
     * <b>Important:</b> If map contains sub maps consider using {@link FlatMap#flatMap} before doing a diff
     * because {@code equals()} on Map isn't ideal.
     *
     * Order of items in collection is ignored
     *
     * @param newMap Maps with new values
     * @param oldMap Maps with old values
     * @return Map (key, value) with old values that differs from new values
     */
    public static Map<String, Object> diff(Map<String, Object> newMap, Map<String, Object> oldMap) {
        Set<String> removedKeys = getRemovedKeys(newMap, oldMap);
        Set<String> addedKeys = getAddedKeys(newMap, oldMap);
        Set<Map.Entry<String, Object>> changedEntries = getChangedEntries(newMap, oldMap, addedKeys);

        Map<String, Object> diff = new HashMap<>();

        addedKeys.forEach(a -> diff.put(a, null));
        removedKeys.forEach(a -> diff.put(a, oldMap.get(a)));
        changedEntries.forEach(a -> diff.put(a.getKey(), oldMap.get(a.getKey())));

        return diff;
    }

    private static Set<String> getRemovedKeys(Map<String, Object> newMap, Map<String, Object> oldMap) {
        return removeSameKeys(oldMap, newMap);
    }

    private static Set<String> getAddedKeys(Map<String, Object> newMap, Map<String, Object> oldMap) {
        return removeSameKeys(newMap, oldMap);
    }

    private static Set<String> removeSameKeys(Map<String, Object> original, Map<String, Object> withSameKeys) {
        Set<String> addedKeys = new HashSet<>(original.keySet());
        addedKeys.removeAll(withSameKeys.keySet());
        return addedKeys;
    }

    private static Set<Map.Entry<String, Object>> getChangedEntries(Map<String, Object> newMap,
                                                                    Map<String, Object> oldMap,
                                                                    Set<String> addedKeys) {

        Set<Map.Entry<String, Object>> changedEntries = new HashSet<>(newMap.entrySet());

        for (Map.Entry<String, Object> entry : newMap.entrySet()) {
            boolean remove;

            // Convert to set - ignore order in lists
            if (entry.getValue() instanceof Collection) {
                Collection oldCollection = (Collection) oldMap.get(entry.getKey());

                remove = oldCollection != null && CollectionUtils.isEqualCollection(
                        new HashSet<>((Collection) entry.getValue()),
                        new HashSet<>(oldCollection));
            } else {
                remove = Objects.equals(entry.getValue(), oldMap.get(entry.getKey()));
            }

            if (remove) {
                changedEntries.remove(entry);
            }
        }

        changedEntries.removeAll(changedEntries
                .stream()
                .filter(e -> addedKeys.contains(e.getKey()))
                .collect(toSet()));

        return changedEntries;
    }

}
