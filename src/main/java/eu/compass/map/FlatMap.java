package eu.compass.map;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Decorates Map with flatten functionality
 */
public class FlatMap {

    private final Map<String, Object> flatMap;

    private FlatMap(Map<String, Object> flatMap) {
        this.flatMap = flatMap;
    }

    /**
     * Flattens the input map
     *
     * Keys are concatenated using dot as a separator.
     *
     * Sub maps has to have String key
     *
     * If there is a Collection containing maps as items, they're flatten. To {@code not flat} something inside
     * collection add {@code collectionKey.notFlatInItemKey} to {@code notFlat}
     *
     * <b>All collections are converted to {@link ArrayList}</b>
     *
     * @param map Map to be flatten
     * @param ignoredKeys Keys containing sub map which will be omitted from composite key
     * @param notFlat Keys containing sub map which won't be flatten
     * @return Flatten map
     */
    public static Map<String, Object> flatten(Map<String, Object> map, Set<String> ignoredKeys, Set<String> notFlat) {
        return flatten("", map, ignoredKeys, notFlat);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> flatten(String root, Map<String, Object> map,
                                               Set<String> ignoredKeys, Set<String> notFlat) {
        Map<String, Object> flatMap = new LinkedHashMap<>();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = StringUtils.isBlank(root) ? entry.getKey() : root + "." + entry.getKey();

            if (entry.getValue() instanceof Map) {
                Map<String, Object> subMap = (Map<String, Object>) entry.getValue();
                if (ignoredKeys.contains(key)) {
                    flatMap.putAll(flatten(root, subMap, ignoredKeys, notFlat));
                } else if (notFlat.contains(key)) {
                    flatMap.put(key.replaceAll("\\." + entry.getKey(), ""), flatten("", subMap, ignoredKeys, notFlat));
                } else {
                    flatMap.putAll(flatten(key, subMap, ignoredKeys, notFlat));
                }
            } else if (entry.getValue() instanceof Collection) {
                flatMap.put(key, flattenCollection(ignoredKeys, notFlat, entry));
            } else {
                flatMap.put(key, entry.getValue());
            }
        }

        return flatMap;
    }

    @SuppressWarnings("unchecked")
    private static List<Object> flattenCollection(Set<String> ignoredKeys, Set<String> notFlat, Map.Entry<String, Object> entry) {
        List<Object> modified = new ArrayList<>(((Collection) entry.getValue()).size());
        for (Object o : (Collection) entry.getValue()) {
            if (o instanceof Map) {
                modified.add(FlatMap.flatten(
                        (Map<String, Object>) o,
                        ignoredKeys.stream().map(s -> s.replaceAll(entry.getKey() + ".", "")).collect(Collectors.toSet()),
                        notFlat.stream().map(s -> s.replaceAll(entry.getKey() + ".", "")).collect(Collectors.toSet())));
            } else {
                modified.add(o);
            }
        }
        return modified;
    }
}

