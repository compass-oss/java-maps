package eu.compass.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Expands keys with dots to sub maps
 *
 * It's basically inverted operation for {@link FlatMap#flatten(String, Map, Set, Set)}
 *
 * Example:
 * key.subKey1: 10,
 * key.subKey2: 20,
 * key2: 30
 *
 * will become
 * key { subKey1: 10, subKey: 20 }, key2: 30
 */
public class ExpandedMap {

    @SuppressWarnings("unchecked")
    public static Map<String, Object> expand(Map<String, Object> flatMap) {
        Map<String, Object> expanded = new HashMap<>();

        for (Map.Entry<String, Object> item : flatMap.entrySet()) {
            if (!item.getKey().contains(".")) {
                if (item.getValue() instanceof Collection) {
                    Collection<Map<String, Object>> subMaps = new ArrayList<>();
                    for (Object o : (Collection) item.getValue()) {
                        if (o instanceof Map) {
                            subMaps.add(expand((Map<String, Object>) o));
                        }
                    }
                    expanded.put(item.getKey(), subMaps);
                } else if (item.getValue() instanceof Map) {
                    expanded.put(item.getKey(), expand((Map<String, Object>)item.getValue()));
                } else {
                    expanded.put(item.getKey(), item.getValue());
                }
            } else {
                String[] compositeKey = item.getKey().split("\\.");
                Map<String, Object> subMap;
                if (!expanded.containsKey(compositeKey[0])) {
                    subMap = new HashMap<>();
                    expanded.put(compositeKey[0], subMap);
                } else {
                    subMap = (Map<String, Object>) expanded.get(compositeKey[0]);
                }

                if (item.getValue() instanceof Map) {
                    subMap.putAll(expand((Map<String, Object>) item.getValue()));
                } else if (item.getValue() instanceof Collection) {
                    Collection<Map<String, Object>> subMaps = new ArrayList<>();
                    for (Object o : (Collection) item.getValue()) {
                        if (o instanceof Map) {
                            subMaps.add(expand((Map<String, Object>) o));
                        }
                    }
                } else {
                    subMap.put(compositeKey[1], item.getValue());
                }
            }
        }



        return expanded;
    }

}
