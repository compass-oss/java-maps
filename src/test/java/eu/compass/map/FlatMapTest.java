package eu.compass.map;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class FlatMapTest {

    @Test
    public void subMapsAreFlatten() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new HashMap<String, Object>() {{
                        put("subKey1", "subValue1");
                    }});
                }}, Collections.<String>emptySet(), Collections.emptySet()),
                is(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2.subKey1", "subValue1");
                }})
        );
    }

    @Test
    public void ignoredKeyIsOmittedFromCompositeKey() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new HashMap<String, Object>() {{
                        put("subKey1", "subValue1");
                    }});
                }}, Collections.singleton("key2"), Collections.emptySet()),
                is(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("subKey1", "subValue1");
                }})
        );
    }

    @Test
    public void keysDefinedAsNotFlatAreNotFlatten() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new HashMap<String, Object>() {{
                        put("subKey1", "subValue1");
                    }});
                }}, Collections.emptySet(), Collections.singleton("key2")),
                is(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new HashMap<String, Object>() {{
                        put("subKey1", "subValue1");
                    }});
                }})
        );
    }

    @Test
    public void eachItemOfCollectionIsFlatten() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new LinkedList<Map<String, Object>>() {{
                        add(new HashMap<String, Object>() {{
                            put("subKey1", new HashMap<String, Object>() {{
                                put("subValue1", "x");
                            }});
                        }});
                    }});
                }}, Collections.emptySet(), Collections.emptySet()),
                is(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new ArrayList<Map<String, Object>>() {{
                        add(new HashMap<String, Object>() {{
                            put("subKey1.subValue1", "x");
                        }});
                    }});
                }}));
    }

    @Test
    public void itemsInsideCollectionAreNotTouchedIfTheyAreNotMap() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key2", Collections.singletonList("value"));
                }}, Collections.emptySet(), Collections.emptySet()),
                is(new HashMap<String, Object>() {{
                    put("key2", new ArrayList<String>() {{
                        add("value");
                    }});
                }}));
    }

    @Test
    public void itemsInsideCollectionsAreNotFlattedIfMarkedAsNotFlat() {
        assertThat(
                FlatMap.flatten(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new LinkedList<Map<String, Object>>() {{
                        add(new HashMap<String, Object>() {{
                            put("subKey1", new HashMap<String, Object>() {{
                                put("subValue1", "x");
                            }});
                        }});
                    }});
                }}, Collections.emptySet(), Collections.singleton("key2.subKey1")),
                is(new HashMap<String, Object>() {{
                    put("key1", "value1");
                    put("key2", new ArrayList<Map<String, Object>>() {{
                        add(new HashMap<String, Object>() {{
                            put("subKey1", new HashMap<String, Object>() {{
                                put("subValue1", "x");
                            }});
                        }});
                    }});
                }}));
    }
}