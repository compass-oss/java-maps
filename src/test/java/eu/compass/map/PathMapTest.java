package eu.compass.map;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PathMapTest {

    @Test
    public void containFieldWithDots() throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("company.type.code", "CODE");
        params.put("company.role", null);
        params.put("company", "compass");
        params.put("company.users", emptyList());
        PathMap message = new PathMap(singletonMap("contractingParties", params));

        assertThat(message.containsKey("contractingParties"), is(true));
        assertThat(message.containsKey("contractingParties.company.type.code"), is(true));
        assertThat(message.containsKey("contractingParties.company"), is(true));
        assertThat(message.containsKey("contractingParties.company.role"), is(true));
        assertThat(message.containsKey("contractingParties.company.users"), is(true));
        assertThat(message.containsKey("contractingParties.company.users.code"), is(false));
        assertThat(message.containsKey("contractingParties.company.partner.type.code"), is(false));
        assertThat(message.containsKey("1.company.type.code"), is(false));
    }
}