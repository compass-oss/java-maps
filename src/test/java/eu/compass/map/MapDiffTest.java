package eu.compass.map;

import org.junit.jupiter.api.Test;

import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Stream;

public class MapDiffTest {

    @Test
    public void addedKeyIsInOutputWithNullValue() {
        Map<String, Object> diff = MapDiff.diff(new HashMap<String, Object>() {{
            put("key", "changedValue");
        }}, Collections.emptyMap());

        assertThat(diff.size(), is(1));
        assertThat(diff.get("key"), is(nullValue()));
    }

    @Test
    public void removedKeyIsInOutputWithOldValue() {
        Map<String, Object> diff = MapDiff.diff(Collections.emptyMap(),
                new HashMap<String, Object>() {{
                    put("key", "changedValue");
                }});

        assertThat(diff.size(), is(1));
        assertThat(diff.get("key"), is("changedValue"));
    }

    @Test
    public void changedValueIsInOuputWithOldValue() {
        Map<String, Object> diff = MapDiff.diff(
                new HashMap<String, Object>() {{
                    put("key", "changedValue");
                }}, new HashMap<String, Object>() {{
                    put("key", "oldValue");
                }}
        );

        assertThat(diff.size(), is(1));
        assertThat(diff.get("key"), is("oldValue"));
    }

    @Test
    public void changeInCollectionsOutputsOldCollectionWithAllItems() {
        Map<String, Object> diff = MapDiff.diff(
                new HashMap<String, Object>() {{
                    put("key", Stream.of(1, 2, 3).collect(toSet()));
                }}, new HashMap<String, Object>() {{
                    put("key", Stream.of(1, 3).collect(toSet()));
                }}
        );

        assertThat(diff.size(), is(1));
        assertThat(diff.get("key"), is(Stream.of(1, 3).collect(toSet())));
    }

    @Test
    public void diffIgnoresOrderInCollection() {
        assertThat(
                MapDiff.diff(new HashMap<String, Object>() {{
                                 put("columns",
                                         new LinkedList<Map<String, Object>>() {
                                             {
                                                 add(new LinkedHashMap<String, Object>() {{
                                                     put("name", "name");
                                                     put("type", "text");
                                                     put("restriction", null);
                                                 }});
                                                 add(new LinkedHashMap<String, Object>() {{
                                                     put("name", "jobTitle.code");
                                                     put("type", "keyword");
                                                     put("restriction", null);
                                                 }});
                                                 add(new LinkedHashMap<String, Object>() {{
                                                     put("name", "jobTitle.order");
                                                     put("type", "integer");
                                                     put("restriction", null);
                                                 }});
                                             }
                                         });
                             }},
                        new HashMap<String, Object>() {{
                            put("columns",
                                    new ArrayList<Map<String, Object>>() {
                                        {
                                            add(new HashMap<String, Object>() {{
                                                put("name", "jobTitle.code");
                                                put("type", "keyword");
                                                put("restriction", null);
                                            }});
                                            add(new HashMap<String, Object>() {{
                                                put("name", "name");
                                                put("type", "text");
                                                put("restriction", null);
                                            }});
                                            add(new HashMap<String, Object>() {{
                                                put("name", "jobTitle.order");
                                                put("type", "integer");
                                                put("restriction", null);
                                            }});
                                        }
                                    });
                        }}),
                is(Collections.emptyMap()));
    }
}