package eu.compass.map;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ExpandedMapTest {

    @Test
    public void keysWithDotAreConvertedToSubMaps() {
        assertThat(ExpandedMap.expand(new HashMap<String, Object>() {{
            put("key.subKey1", 10);
            put("key.subKey2", 20);
            put("key2", "test");
        }}), is(new HashMap<String, Object>() {{
            put("key", new HashMap<String, Object>() {{
                put("subKey1", 10);
                put("subKey2", 20);
            }});
            put("key2", "test");
        }}));
    }

    @Test
    public void collectionItemsAreExpandedToSubMaps() {
        assertThat(ExpandedMap.expand(new HashMap<String, Object>() {{
            put("keys", Stream.of(
                    new HashMap<String, Object>() {{
                        put("subKey.foo", 1);
                        put("subKey.bar", 2);
                    }},
                    new HashMap<String, Object>() {{
                        put("subKey.foo", 3);
                        put("subKey.bar", 4);
                    }}).collect(toList()));
        }}), is(new HashMap<String, Object>() {{
            put("keys", Stream.of(
                    new HashMap<String, Object>() {{
                        put("subKey", new HashMap<String, Object>() {{
                            put("foo", 1);
                            put("bar", 2);
                        }});
                    }},
                    new HashMap<String, Object>() {{
                        put("subKey", new HashMap<String, Object>() {{
                            put("foo", 3);
                            put("bar", 4);
                        }});
                    }}
            ).collect(toList()));
        }}));
    }
}