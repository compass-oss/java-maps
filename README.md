# Java Maps
Util library fro Java `Map<K,V>` adds methods which are not available in standard library.

Currently supports:

## Flat map
Returns flatted map eg.

```json
{
    "foo": {
        "bar": {
            "id": 10,
            "foo": {
                "name": "xyz"
            }
        },
        "baz": [
          { 
            "foo": {
              "x": 42
            }
          }
        ]
    },
    "active": false
}
```

will be flattened into

```json
{
    "foo.bar.id": 10,
    "foo.bar.foo.name": "xyz",
    "foo.baz": [
      {
        "foo.x": 42
      }
    ],
    "active": false
}
```

Map can be _unflatten_ by using `ExpandedMap#expand(Map<Srting,Object> flatMap)`

## Diff on maps
Can find differences between two maps eg.

Diff between (new value)

```json
{
    "foo": "Value",
    "bar": 42
}
```

and (old value)

```json
{
    "foo": "Value",
    "baz": 35
}
```

will be

```json
{
    "bar": null,
    "baz": 35
}
```

as old values are outputted.

If Map contains a sub-map it can be good to flat those map at first because equals on maps can produce weird output

## Path map
Finds data in map by path with dots

```json
{
    "foo": {
        "bar": {
            "id": 10,
            "foo": {
                "name": "xyz"
            }
        },
        "ppp.id": 1
    },
    "active": false
}
```

Example

-----------------
| path | result |
------------------
|foo.bar.id| 10 |
-----------------
|foo.ppp| null|
----------------
|foo,oo.id|1|
---------------
|foo.bar| {"id": .., "foo" :...}|
-----------
